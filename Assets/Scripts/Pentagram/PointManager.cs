﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointManager : MonoBehaviour {

	public PentagramManager pentagram;

	void OnMouseDown() {
		pentagram.StartIncantation ();
	}

	void OnMouseOver() {
		pentagram.AddPoint (gameObject);
	}

	void OnMouseUp() {
		pentagram.StopIncantation ();
	}
}
