﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PentagramManager : Singleton<PentagramManager> {

	List<GameObject> Symbol = new List<GameObject>();
	bool IsIncanting = false;
	GameObject PreviousPentagramItem = null;

	public SpellManager SpellManagerObject;
	public GameObject SelfCastSpellCase;
	public GameObject OnOtherSpellCase;

	public Timer timer;
	public MunitionManager munition;

	// Called when the player clicks on a PentagramItem
	public void StartIncantation() {
		IsIncanting = true;
	}

	// Called when the player hovers over a PentagramItem
	public void AddPoint(GameObject PentagramItem) {
		if (IsIncanting && !Symbol.Contains(PentagramItem)) {
			Symbol.Add (PentagramItem);

			AddHalo (PentagramItem);

			if (PreviousPentagramItem != null) {
				DrawLinesBetweenPoints (PentagramItem);
			}

			PreviousPentagramItem = PentagramItem;

			if(Symbol.Count == 5) {
				LaunchSpell ();
			}
		}
	}

	private void DrawLinesBetweenPoints(GameObject PentagramItem) {
		LineRenderer LineOfPower = PreviousPentagramItem.GetComponent<LineRenderer> ();

		var positions = new Vector3[2];
		positions [0] = PreviousPentagramItem.transform.position;
		positions [1] = PentagramItem.transform.position;

		LineOfPower.SetPositions (positions);
	}

	private void AddHalo(GameObject PentagramItem) {
		Behaviour Halo = (Behaviour)PentagramItem.GetComponent ("Halo");
		Halo.enabled = true;
	}	

	// Called when the player stops hovering over the PentagramItems or the Symbol is finished
	public void StopIncantation() {
		Symbol.RemoveAll (EmptySymbol);
		IsIncanting = false;
		PreviousPentagramItem = null;
	}

	//Predicate that always returns true and clears the Halo
	private static bool EmptySymbol(GameObject PentagramItem) {
		Behaviour Halo = (Behaviour)PentagramItem.GetComponent ("Halo");
		Halo.enabled = false;

		LineRenderer LineOfPower = PentagramItem.GetComponent<LineRenderer> ();
		var positions = new Vector3[2];
		positions [0] = new Vector3(0f, 0f, 0f);
		positions [1] = new Vector3(0f, 0f, 0f);
		LineOfPower.SetPositions (positions);

		return true;
	}

	// To Rename
	void LaunchSpell() {
		string Suit = "";

		foreach (var item in Symbol) {
			Suit += item.GetComponent<Point>().GetId();
		}

		Spell spell = SpellManagerObject.getSpell (Suit);
		if (spell != null) {
			if (spell is OnOtherSpell) {
				OnOtherSpellCase.GetComponent<SpellCase> ().SetSpell(spell);
				munition.initialize(((OnOtherSpell) spell).nbUse);
			} else {
				SelfCastSpellCase.GetComponent<SpellCase> ().SetSpell(spell);
				timer.initialize (((SelfCastSpell) spell).duration);
			}
		} else {
			Debug.Log ("Spell doesn't exist!");
		}
			
		StopIncantation ();

	}

}
