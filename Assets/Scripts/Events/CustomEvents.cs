﻿using UnityEngine.Events;

namespace Littlesorcerer.Events {
	[System.Serializable]
	public class TextEvent : UnityEvent<string>{};
}