﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Spells/SelfCastSpell/JumpSpell")]
public class JumpSpell : SelfCastSpell {

	public float Boost;
	float InitJump;
		
	public override void Effect() {
		Player player = GameObject.Find ("Player").GetComponent<Player>();
		InitJump = player.Jump;
		player.Jump *= Boost;
	}

	public override void Reset() {
		Player player = GameObject.Find ("Player").GetComponent<Player>();
		player.Jump = InitJump;
	}
}
