﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Littlesorcerer.Events;

public class SpellCase : MonoBehaviour {
	
	Spell spell;
	Timer timer;

	public TextEvent OnChangeSpell;

	public void SetSpell(Spell newSpell) {
		spell = newSpell;
		if (spell != null) {
			OnChangeSpell.Invoke (spell.ToString ());
		} else {
			OnChangeSpell.Invoke ("");
		}
	}

	public void Reset() {
		spell = null;
		OnChangeSpell.Invoke ("");
	}

	public Spell GetSpell() {
		return spell;
	}

	void OnEnable() {
		Reset ();
	}
}