﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Spells", order = 1)]
public abstract class Spell : ScriptableObject {

	public string title;
	public string description;
	public string suit;
		
	public abstract void Effect();

}