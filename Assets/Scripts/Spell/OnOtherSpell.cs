﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Spells/OnOtherSpell")]
public abstract class OnOtherSpell : Spell {
	
	public int damage;
	public int velocity;
	public float nbUse;
	public int distanceMax;

}