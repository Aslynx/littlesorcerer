﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellManager : Singleton<SpellManager> {

	public Spell[] spells;

	public bool exists(string Suit){
		foreach (var spell in spells) {
			if (spell.suit.Equals (Suit))
				return true;
		}
		return false;
	}

	public Spell getSpell (string Suit) {
		foreach (var spell in spells) {
			if (spell.suit.Equals (Suit))
				return spell;
		}
		return null;
	}

}
