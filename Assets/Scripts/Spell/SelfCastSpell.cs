﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Spells/SelfCastSpell")]
public abstract class SelfCastSpell : Spell {

	public float duration;

	public abstract void Reset ();
}