﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	Image progress;

	public float run;
	public float speed=0;
	public bool check=false;
	SpellCase sp;

	void Start () {
		progress = gameObject.GetComponent<Image>();
		progress.enabled = false;
		sp = FindObjectsOfType<SpellCase> ()[0];
	}	

	public void initialize(float run) {
		progress.fillAmount = 100f;
		progress.enabled = true;
		this.run = Mathf.Pow(run, -1f);
		Debug.Log (this.run);
		Debug.Log (run);
		check = false;
	}

	public void SetCheck(){
		check = true;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			SetCheck ();
		}

		if (check) {
			if(progress.fillAmount == 0) {
				StopAllCoroutines ();
				sp.Reset ();
			} else {
				StartCoroutine (delay());
			}
		}
	}

	IEnumerator delay() {
		progress.fillAmount = progress.fillAmount - run;

		yield return new WaitForSeconds (speed);
	}
}
