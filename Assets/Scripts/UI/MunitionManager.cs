﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MunitionManager : MonoBehaviour {
	Image munition;

	public float run=0.1f;
	public float speed=0;
	SpellCase sp;

	// Use this for initialization
	void Start () {
		munition = gameObject.GetComponent<Image>();
		munition.enabled = false;
		sp = FindObjectsOfType<SpellCase> ()[1];
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire2")) {
			StartCoroutine (shoot());
			if (Mathf.Approximately(munition.fillAmount, 0f)) {
				sp.Reset ();
			}
		} else {
			StopAllCoroutines ();
		}
	}

	public void initialize(float nbUse) {
		munition.fillAmount = 100f;
		munition.enabled = true;
		this.run = Mathf.Pow(nbUse, -1f);
	}

	IEnumerator shoot() {
		munition.fillAmount = munition.fillAmount - run;
		Debug.Log (munition.fillAmount);

		yield return new WaitForSeconds (speed);
	}
}
