﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : Singleton<SceneLoader> {

	public string scene1;
	public string scene2;

	public void Start () {
		SceneManager.LoadScene (scene1, LoadSceneMode.Additive);
	}

	public void Update () {
		if(Input.GetKeyDown("i")) {
			SceneManager.UnloadSceneAsync (scene1);
			SceneManager.LoadScene (scene2, LoadSceneMode.Additive);	
		}

		if (Input.GetKeyDown("j")) {
			SceneManager.UnloadSceneAsync (scene2);
			SceneManager.LoadScene (scene1, LoadSceneMode.Additive);
		}

	}

	public void Teleportation(string tp) {
		Debug.Log ("Tp touché : " + tp);
	}
}
