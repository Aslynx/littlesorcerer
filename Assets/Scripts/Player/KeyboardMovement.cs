﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMovement : MonoBehaviour {

	[SerializeField]
	float speed = 1f;

	[SerializeField]
	float height = 0f;

	// Update is called once per frame
	void Update () {
			Move ();
			Jump ();

	}

	void Move() {
		Vector3 movement = Vector3.zero;
		movement.x = Input.GetAxisRaw ("Horizontal");
		movement *= speed;

		movement *= Time.deltaTime;

		transform.Translate (movement);
	}

	void Jump() {
		Vector3 movement = Vector3.zero;
		movement.y += Input.GetAxisRaw ("Vertical");

		movement *= height;

		movement *= Time.deltaTime;

		transform.Translate (movement);
	}

	public void ChangeHeight(float boost) {
		height *= boost;
	}
}
