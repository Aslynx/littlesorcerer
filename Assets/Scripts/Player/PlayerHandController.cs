﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHandController : MonoBehaviour
{
	public SpellCase SelfCastSpellCase;
	public SpellCase OnOtherSpellCase;
	GameObject[] goArray;
	bool hasBeenLoaded = false;
	string playerUI = "PlayerUI";

	void Start() {

	}

	// Update is called once per frame
	void Update () {
		if (!hasBeenLoaded) {
			LoadSpellCases ();
		} else {
			if (Input.GetButtonDown ("Jump")) {
				LaunchSelfCastSpell ();
			}
			if (Input.GetButtonDown ("Fire2")) {
				LaunchOnOtherSpell ();
			}
		}
	}

	void LoadSpellCases() {
		Scene scene = SceneManager.GetSceneByName (playerUI);
		if (scene.isLoaded) {
			goArray = scene.GetRootGameObjects ();

			GameObject spellCases = goArray[1];

			SelfCastSpellCase = spellCases.GetComponentsInChildren<SpellCase> ()[0];
			OnOtherSpellCase = spellCases.GetComponentsInChildren<SpellCase> ()[1];

			hasBeenLoaded = true;
		}
	}

	void LaunchSelfCastSpell() {
		if(SelfCastSpellCase.GetSpell() != null) {
			Debug.Log (SelfCastSpellCase.GetSpell().ToString());	
		}
	}

	void LaunchOnOtherSpell() {
		if (OnOtherSpellCase.GetSpell () != null) {
			Debug.Log (OnOtherSpellCase.GetSpell ().ToString ());
		}
	}
}

